package main;

public class MDynamicArray<I>{
    private I[] arr;

    public void setArray(I[] arr) {
        this.arr = arr;
    }

    public void add(I a, int ind) {
        if (ind < 0){
            System.out.println("Введен отрицательный индекс");
            return;
        }

        I[] buf = (I[]) new Object[arr.length + 1];

        if (arr.length == 0){
            buf[0] = a;
            this.arr = buf;
            return;
        }

        if (ind > arr.length){
            addEnd(a);
            return;
        }

        for (int i = 0; i < buf.length; i++) {
            if (i < ind){
                buf[i] = arr[i];
            }
            if (i > ind){
                buf[i] = arr[i-1];
            }
        }
        buf[ind] = a;
        arr = buf;
    }

    public void addArray(I[] inn) {
        if (arr.length == 0) {
            arr = inn;
            return;
        }
        for (int i = 0; i < inn.length; i++) {
            addEnd(inn[i]);
        }
    }

    public void addStart(I a) {
        I[] buf = (I[]) new Object[arr.length + 1];
        buf[0] = a;
        for (int i = 0; i < arr.length; i++) {
            buf[i + 1] = arr[i];
        }
        arr = buf;
    }

    public void addEnd(I a) {
        I[] buf = (I[]) new Object[arr.length + 1];
        buf[buf.length - 1] = a;
        for (int i = 0; i < arr.length; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    public void del(int ind) {
        if (ind < 0){
            System.out.println("Введен отрицательный индекс");
            return;
        }

        if (arr.length == 0){
            System.out.println("Удаление элемента невозможно: массив пустой");
            return;
        }

        if (ind > arr.length){
            System.out.println("Удаление элемента невозможно: в массиве меньше элементов");
            return;
        }

        I[] buf = (I[]) new Object[arr.length - 1];

        for (int i = 0; i < arr.length; i++) {
            if (i < ind){
                buf[i] = arr[i];
            }
            if (i > ind){
                buf[i - 1] = arr[i];
            }
        }
        arr = buf;
    }

    public void delStart() {
        I[] buf = (I[]) new Object[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i + 1];
        }
        arr = buf;
    }

    public void delEnd() {
        I[] buf = (I[]) new Object[arr.length - 1];
        for (int i = 0; i < arr.length - 1; i++) {
            buf[i] = arr[i];
        }
        arr = buf;
    }

    public void clear() {
        arr = (I[]) new Object[0];
    }

    public I[] getArray() {
        return arr;
    }

    public String intoString() {
        String str = new String();
        if(arr.length != 0){
            str += "[";
            for (int i = 0; i < arr.length-1; i++) {
                str += arr[i] + ", ";
            }
            str += arr[arr.length-1] + "]";
        } else {
            str = "Массив пустой";
        }
        return str;
    }

    public void print() {
        if (arr.length != 0) {
            System.out.print("[");
            for (int i = 0; i < arr.length - 1; i++) {
                System.out.print(arr[i] + ", ");
            }
            System.out.println(arr[arr.length - 1] + "]");
        } else {
            System.out.println("Массив пустой");
        }
    }
}
