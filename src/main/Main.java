package main;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        MDynamicArray<String> mDynamicArrayFirst = new MDynamicArray<>();

        mDynamicArrayFirst.setArray(new String[]{"1", "2", "3"}); mDynamicArrayFirst.print();
        mDynamicArrayFirst.addArray(new String[]{"4", "5", "6"}); mDynamicArrayFirst.print();
        mDynamicArrayFirst.add("13", 5); mDynamicArrayFirst.print();
        mDynamicArrayFirst.addStart("0"); mDynamicArrayFirst.print();
        mDynamicArrayFirst.addEnd("75"); mDynamicArrayFirst.print();
        mDynamicArrayFirst.del(6); mDynamicArrayFirst.print();
        mDynamicArrayFirst.delEnd(); mDynamicArrayFirst.print();
        mDynamicArrayFirst.delStart(); mDynamicArrayFirst.print();
        System.out.println(mDynamicArrayFirst.intoString());
        System.out.println(Arrays.toString(mDynamicArrayFirst.getArray()));
        mDynamicArrayFirst.clear(); mDynamicArrayFirst.print();

        System.out.println("-----------------------------------------------------------------------------------------");

        MDynamicArray<Integer> mDynamicArraySecond = new MDynamicArray<>();

        mDynamicArraySecond.setArray(new Integer[]{1, 2, 3}); mDynamicArraySecond.print();
        mDynamicArraySecond.addArray(new Integer[]{4, 5, 6}); mDynamicArraySecond.print();
        mDynamicArraySecond.add(13, 5); mDynamicArraySecond.print();
        mDynamicArraySecond.addStart(0); mDynamicArraySecond.print();
        mDynamicArraySecond.addEnd(75); mDynamicArraySecond.print();
        mDynamicArraySecond.del(6); mDynamicArraySecond.print();
        mDynamicArraySecond.delEnd(); mDynamicArraySecond.print();
        mDynamicArraySecond.delStart(); mDynamicArraySecond.print();
        System.out.println(mDynamicArraySecond.intoString());
        System.out.println(Arrays.toString(mDynamicArraySecond.getArray()));
        mDynamicArraySecond.clear(); mDynamicArraySecond.print();

        System.out.println("-----------------------------------------------------------------------------------------");

        MDynamicArray<Double> mDynamicArrayThird = new MDynamicArray<>();

        mDynamicArrayThird.setArray(new Double[]{1.5, 2.5, 3.6}); mDynamicArrayThird.print();
        mDynamicArrayThird.addArray(new Double[]{4.1, 5.8, 6.6}); mDynamicArrayThird.print();
        mDynamicArrayThird.add(13.6, 5); mDynamicArrayThird.print();
        mDynamicArrayThird.addStart(1.1); mDynamicArrayThird.print();
        mDynamicArrayThird.addEnd(75.6); mDynamicArrayThird.print();
        mDynamicArrayThird.del(6); mDynamicArrayThird.print();
        mDynamicArrayThird.delEnd(); mDynamicArrayThird.print();
        mDynamicArrayThird.delStart(); mDynamicArrayThird.print();
        System.out.println(mDynamicArrayThird.intoString());
        System.out.println(Arrays.toString(mDynamicArrayThird.getArray()));
        mDynamicArrayThird.clear(); mDynamicArrayThird.print();
    }
}
